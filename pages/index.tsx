import { GetServerSideProps } from "next";
import Head from "next/head";
import Image from "next/image";
import { FC } from "react";
import { GitlabLink } from "../components/GitlabLink";
import { Section } from "../components/Section";
import WebstoreLinksGrid from "../components/WebstoreLinksGrid";

type HomeProps = {
  contributors: Array<string>;
};

const Home: FC<HomeProps> = ({ contributors }) => {
  return (
    <>
      <Head>
        <title>VŠE+</title>
        <meta name="description" content="Rozšíření do prohlížeče, které přidává funkcionality do systému InSIS pro zlepšení quality-of-life studentů Vysoké školy ekonomické v Praze." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <meta name="og:title" content="VŠE+" />
        <meta name="og:description" content="Rozšíření do prohlížeče, které přidává funkcionality do systému InSIS pro zlepšení quality-of-life studentů Vysoké školy ekonomické v Praze." />
        <meta name="og:url" content="https://vsepl.us" />
        <meta name="og:image" content="https://vsepl.us/preview.png" />
        <meta name="og:image:type" content="image/png" />
        <meta name="og:image:width" content="640" />
        <meta name="og:image:height" content="480" />

        <meta name="twitter:title" content="VŠE+" />
        <meta name="twitter:description" content="Rozšíření do prohlížeče, které přidává funkcionality do systému InSIS pro zlepšení quality-of-life studentů Vysoké školy ekonomické v Praze." />
        <meta name="twitter:image" content="https://vsepl.us/preview.png" />
        <meta name="twitter:image:src" content="https://vsepl.us/preview.png" />
        <meta name="twitter:card" content="Vylepšený rozvrh" />

        <meta name="theme-color" content="#8F0200" />

        <link rel="icon" href="/favicon.png" />
      </Head>

      <header className="flex flex-col items-center py-16 bg-neutral-100">
        <div className="flex flex-row items-center pb-10">
          <Image src="/logo.png" alt="VŠE+ Logo" width={64} height={64} quality={100} priority />
          <h1 className="ml-4 text-5xl font-black">VŠE+</h1>
        </div>
        <h2 className="text-lg font-bold text-neutral-500 text-center px-5">Rozšíření do prohlížeče, které přidává funkcionality do systému InSIS</h2>
      </header>

      <main className="flex flex-col items-center">
        <WebstoreLinksGrid />

        <Section title="Přidané funkcionality" className="mt-20">
          <div className="grid grid-cols-1 lg:grid-cols-2 my-10 items-center">
            <div className="mr-5">
              <h2 className="text-center text-red-800 font-black text-3xl mb-4">Vylepšený rozvrh</h2>
              <p className="text-center font-bold w-2/3 mx-auto">
                Automatické zpracování volných dnů a blokových akcí.
                Možnost přepínání mezi týdny a přidávání poznámek ke konkrétním hodinám,
                což má využití např. pro poznamenání úkolů nebo testů.
              </p>
            </div>
            <Image src="/features/enhanced-timetable.png" alt="Vylepšený rozvrh" width={1200} height={675} quality={100} priority className="inline-block w-full" />
          </div>

          <div className="grid grid-cols-1 lg:grid-cols-2 my-10 items-center">
            <Image src="/features/timetable-preview.png" alt="Náhled rozvrhu a detekce kolizí" width={1200} height={675} quality={100} className="inline-block w-full" />
            <div className="ml-5">
              <h2 className="text-center text-red-800 font-black text-3xl mb-4">Náhled &amp; detekce kolizí v registracích</h2>
              <p className="text-center font-bold w-2/3 mx-auto">
                Rozšíření přidává náhled rozvrhu při registraci předmětů s automatickou detekcí kolizí hodin v rozvrhu. <br />
              </p>
            </div>
          </div>

          <div className="grid grid-cols-1 lg:grid-cols-2 my-10 items-center">
            <div className="mr-5">
              <h2 className="text-center text-red-800 font-black text-3xl mb-4">
                Připomenutí odevzdáváren, <br />
                export do kalendáře
              </h2>
              <p className="text-center font-bold w-2/3 mx-auto">
                Možnost nechat si zaslat připomenutí na email přímo z InSISu.
                Export do Google kalendáře nebo do Outlooku jedním klikem.
              </p>
            </div>
            <Image src="/features/submission-reminders.png" alt="Vylepšený rozvrh" width={1200} height={675} quality={100} className="inline-block w-full" />
          </div>
        </Section>

        <Section title="Zdrojové kódy">
          <div className="flex flex-col md:flex-row items-center justify-center gap-10 py-10">
            <GitlabLink name="Rozšíření" repository="vse-plus/extension" />
            <GitlabLink name="API" repository="vse-plus/api" />
          </div>
        </Section>

        <footer className="py-4 text-sm uppercase tracking-wider font-bold text-black">
          &copy; { new Date().getFullYear() } Jiří Vrba 
          <span className="text-gray-400"> + Gitlab contributors ({contributors.join(",")})</span>
        </footer>
      </main>
    </>
  )
}

type Contributor = {
  name: string;
  email: string;
  commits: number;
}

export const getServerSideProps: GetServerSideProps<HomeProps> = async () => {
  const contributors = await fetch("https://gitlab.com/api/v4/projects/41950665/repository/contributors")
      .then(responses => responses.json())
      .then(response => response
        .sort((a: Contributor, b: Contributor) => b.commits - a.commits)
        .map((contributor: Contributor) => contributor.name)
        .filter((contributor: string) => contributor !== "Jiří Vrba")
      );

  return {
    props: {
      contributors
    }
  }
};

export default Home;