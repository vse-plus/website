import { type ReactNode, type FC } from "react";

export type SectionProps = {
    title: string;
    children?: ReactNode;
    className?: string;
};

export const Section: FC<SectionProps> = ({ title, children, className = "" }: SectionProps) => {
    return (
        <section className={`container ${className}`}>
            <h2 className="text-4xl font-black tracking-wide uppercase text-center mb-5">{ title }</h2>
            {children}
        </section>
    );
};