import { type FC } from "react";
import Image from "next/image";

type WebstoreType = "firefox" | "chrome" | "brave" | "edge";

type WebstoreLinkProps = {
    type: WebstoreType;
    href: string;
    highlighted?: boolean;
}

const names: Record<WebstoreType, string> = {
    firefox: "Mozilla Firefox",
    chrome: "Google chrome",
    brave: "Brave",
    edge: "Microsoft Edge"
};

const sources: Record<WebstoreType, string> = {
    firefox: "Mozilla Addons",
    chrome: "Google web store",
    brave: "Google web store",
    edge: "Google web store",
}

const WebstoreLink: FC<WebstoreLinkProps> = ({ type, href, highlighted = false }: WebstoreLinkProps) => {
    return (
        <a href={href} target="_blank" rel="noreferrer" className={`flex flex-row items-center p-8 rounded-xl bg-white shadow-sm transform transition-all hover:shadow-xl overflow-hidden ${highlighted ? "bg-gradient-to-r from-black to-red-800 group relative hover:scale-105" : "bg-white border"}`}>
            {
                highlighted && <div className="absolute bg-white bg-opacity-40 w-full h-48 transition-all duration-1000 transform -rotate-90 -left-full group-hover:left-full group-hover:h-64"></div>
            }
            <Image src={`/browser/${type}.svg`} alt={type} width={highlighted ? 48 : 32} height={highlighted ? 48 : 32} />
            <div className="ml-8 flex flex-col items-start">
                <div className={`font-black mb-1 ${highlighted ? "text-white text-3xl" : "text-black text-xl"}`}>{names[type]}</div>
                <div className={`text-xs uppercase tracking-widest font-bold ${highlighted ? "text-white opacity-80" : "text-neutral-400"}`}>{sources[type]}</div>
            </div>
        </a>
    );
};

export default WebstoreLink;