import { useEffect, useState, type FC } from "react";
import WebstoreLink from "./WebstoreLink";

const WebstoreLinksGrid: FC = () => {
    const [userAgent, setUserAgent] = useState<string>("");
    
    useEffect(() => { setUserAgent(window.navigator.userAgent) }, []);

    return (
        <div className="w-full lg:w-1/2 grid grid-cols-1 md:grid-cols-2 gap-5 mt-10 px-10">
            <WebstoreLink type="firefox" href="https://addons.mozilla.org/en-US/firefox/addon/v%C5%A1e/" highlighted={/Firefox\//.test(userAgent)}/>
            <WebstoreLink type="chrome" href="https://chrome.google.com/webstore/detail/v%C5%A1e%2B/hbngcjlobkadngdbbaknlgmholembfbj" highlighted={/Chrome\//.test(userAgent) && !/EdgA?\//.test(userAgent) && !userAgent.includes("Brave")} />
            <WebstoreLink type="brave" href="https://chrome.google.com/webstore/detail/v%C5%A1e%2B/hbngcjlobkadngdbbaknlgmholembfbj" highlighted={userAgent.includes("Brave")}/>
            <WebstoreLink type="edge" href="https://chrome.google.com/webstore/detail/v%C5%A1e%2B/hbngcjlobkadngdbbaknlgmholembfbj" highlighted={/EdgA?\//.test(userAgent)}/>
        </div>
    );
};


export default WebstoreLinksGrid;